import React from 'react';

import './style.css';

function Map() {
  return <div className={`map ${props.className}`} style={props.style}></div>;
}

export default Map;
