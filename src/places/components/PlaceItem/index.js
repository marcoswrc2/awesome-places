import React, { useState } from 'react';

import Card from '../../../shared/components/UIElements/Card/Card';
import Button from '../../../shared/components/FormElements/Button/index';
import Modal from '../../../shared/components/UIElements/Modal';
import './style.css';

function PlaceItem(props) {
  const [showMap, setShowMap] = useState(false);
  const [confirmModal, setConfirmModal] = useState(false);

  const openMapHandler = () => setShowMap(true);
  const closeMapHandler = () => setShowMap(false);
  const deleteWarnignHandler = () => {
    setConfirmModal(true);
  };
  const cancelDeleteHandler = () => {
    setConfirmModal(false);
  };
  const confirmDeleteHandler = () => {
    console.log('canceling...');
  };

  return (
    <React.Fragment>
      <Modal
        show={showMap}
        onCancel={closeMapHandler}
        header={props.address}
        contentClass='place-item__modal-content'
        footerClass='place-item__modal-actions'
        footer={<Button onClick={closeMapHandler}>CLOSE</Button>}
      >
        <div className='map-container'>
          <h2>THE MAP</h2>
        </div>
      </Modal>
      <Modal
        show={confirmModal}
        onCancel={cancelDeleteHandler}
        header='Are you sure?'
        footerClass='place-item__modal-actions'
        footer={
          <>
            <Button onClick={cancelDeleteHandler} inverse>
              CANCEL
            </Button>
            <Button onClick={confirmDeleteHandler} danger>
              CONFIRM
            </Button>
          </>
        }
      >
        <p>Do you want to proceed and delete this place ? Please note that it can't be undone thereafter.</p>
      </Modal>
      <li className='place-item'>
        <Card className='place-item__content'>
          <div className='place-item__image'>
            <img src={props.image} alt={props.title} />
          </div>
          <div className='place-item__info'>
            <h2>{props.title}</h2>
            <h3>{props.address}</h3>
            <p>{props.description}</p>
          </div>
          <div className='place-item__actions'>
            <Button inverse onClick={openMapHandler}>
              VIEW ON MAP
            </Button>
            <Button to={`/places/${props.id}`}>EDIT</Button>
            <Button onClick={deleteWarnignHandler} danger>
              DELETE
            </Button>
          </div>
        </Card>
      </li>
    </React.Fragment>
  );
}

export default PlaceItem;
