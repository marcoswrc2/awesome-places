import React from 'react';
import { useParams } from 'react-router-dom';

import PlaceList from '../components/PlaceList/index';
import Button from '../../shared/components/FormElements/Button';

const dummy_places = [
  {
    id: 'p1',
    title: 'Cristo Redentor',
    description: 'O maior símbolo arquitetônico da cidade do Rio de Janeiro',
    imageUrl:
      'https://images.unsplash.com/photo-1548533701-a1e8bd19f9ce?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=345&q=80',
    address: 'Centro, Rio de Janeiro - RJ, 20221-260',
    location: {
      lat: -22.9068417,
      lng: -43.1750852,
    },
    creator: 'u1',
  },
  {
    id: 'p2',
    title: 'Cristo Redentor',
    description: 'O maior símbolo arquitetônico da cidade do Rio de Janeiro',
    imageUrl:
      'https://images.unsplash.com/photo-1548533701-a1e8bd19f9ce?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=345&q=80',
    address: 'Centro, Rio de Janeiro - RJ, 20221-260',
    location: {
      lat: -22.9068417,
      lng: -43.1750852,
    },
    creator: 'u2',
  },
];

const UserPlaces = () => {
  const userId = useParams().userId;
  const filtered = dummy_places.filter((place) => {
    return place.creator === userId;
  });

  return <PlaceList items={filtered} />;
};

export default UserPlaces;
