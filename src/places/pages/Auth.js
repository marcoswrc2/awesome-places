import React, { useState } from 'react';

import './Auth.css';
import Card from '../../shared/components/UIElements/Card/Card';
import Input from '../../shared/components/FormElements/Input';
import Button from '../../shared/components/FormElements/Button';
import { VALIDATOR_EMAIL, VALIDATOR_MINLENGTH, VALIDATOR_REQUIRE } from '../../shared/utils/validators';
import { useForm } from '../../shared/hooks/form-hook';

function Auth() {
  const [formState, inputHandler, setFormData] = useForm(
    { email: { value: '', isValid: false }, password: { value: '', isValid: false } },
    false
  );
  const [loginMode, setLoginMode] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();

  const authSubmitHandler = async (e) => {
    e.preventDefault();
    if (loginMode) {
    } else {
      try {
        setIsLoading(true);
        const response = await fetch('http://localhost:5000/api/users/signup', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            name: formState.inputs.name.value,
            email: formState.inputs.email.value,
            password: formState.inputs.password.value,
          }),
        });
        const responseData = await response.json();
        console.log(responseData);
        setIsLoading(false);
        // auth.login();
      } catch (error) {
        console.log(error);
        setIsLoading(false);
        setError(error.message || 'Something went wrong, please try again.');
      }
    }
  };

  const switchFormHandler = () => {
    if (!loginMode) {
      setFormData(
        { ...formState.inputs, name: null },
        formState.inputs.email.isValid && formState.inputs.password.isValid
      );
    } else {
      setFormData(
        {
          ...formState.inputs,
          name: {
            value: '',
            isValid: false,
          },
        },
        false
      );
    }
    setLoginMode(!loginMode);
  };

  return (
    <Card className='authentication'>
      <h2>Login Required</h2>
      <hr />
      <form onSubmit={authSubmitHandler}>
        {!loginMode && (
          <Input
            element='input'
            id='name'
            type='text'
            label='Your name'
            validators={[VALIDATOR_REQUIRE()]}
            onInput={inputHandler}
            errorText='Please enter a name.'
          />
        )}
        <Input
          element='input'
          id='email'
          type='email'
          label='E-MAIL'
          validators={[VALIDATOR_EMAIL()]}
          errorText='Please enter a valid email address.'
          onInput={inputHandler}
        />
        <Input
          element='input'
          id='password'
          type='password'
          label='Password'
          validators={[VALIDATOR_MINLENGTH(6)]}
          errorText='Please enter a valid password().'
          onInput={inputHandler}
        />
        <Button type='submit' disabled={!formState.isValid}>
          {loginMode ? 'LOGIN' : 'SIGNUP'}
        </Button>
      </form>
      <Button inverse onClick={switchFormHandler}>
        SWITCH TO {loginMode ? 'SIGNUP' : 'LOGIN'}
      </Button>
    </Card>
  );
}

export default Auth;
