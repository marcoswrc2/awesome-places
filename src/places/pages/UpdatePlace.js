import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import Input from '../../shared/components/FormElements/Input';
import Button from '../../shared/components/FormElements/Button';
import { VALIDATOR_REQUIRE, VALIDATOR_MINLENGTH } from '../../shared/utils/validators';
import './NewPlace.css';
import { useForm } from '../../shared/hooks/form-hook';
import Card from '../../shared/components/UIElements/Card/Card';

const dummy_places = [
  {
    id: 'p1',
    title: 'Cristo Redentor',
    description: 'O maior símbolo arquitetônico da cidade do Rio de Janeiro',
    imageUrl:
      'https://images.unsplash.com/photo-1548533701-a1e8bd19f9ce?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=345&q=80',
    address: 'Centro, Rio de Janeiro - RJ, 20221-260',
    location: {
      lat: -22.9068417,
      lng: -43.1750852,
    },
    creator: 'u1',
  },
  {
    id: 'p2',
    title: 'Cristo Redentor2',
    description: 'O maior símbolo arquitetônico da cidade do Rio de Janeiro',
    imageUrl:
      'https://images.unsplash.com/photo-1548533701-a1e8bd19f9ce?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=345&q=80',
    address: 'Centro, Rio de Janeiro - RJ, 20221-260',
    location: {
      lat: -22.9068417,
      lng: -43.1750852,
    },
    creator: 'u2',
  },
];

function UpdatePlace() {
  const placeId = useParams().placeId;
  const [isLoading, setIsLoading] = useState(true);
  const [formState, inputHandler, setFormData] = useForm(
    {
      title: { value: '', isValid: false },
      description: { value: '', isValid: false },
    },
    false
  );
  const identifiedPlace = dummy_places.find((place) => place.id === placeId);

  useEffect(() => {
    if (identifiedPlace) {
      setFormData(
        {
          title: { value: identifiedPlace.title, isValid: true },
          description: { value: identifiedPlace.description, isValid: true },
        },
        true
      );
    }

    setIsLoading(false);
  }, [setFormData, identifiedPlace]);

  const placeUpdateSubmitHandler = (event) => {
    event.preventDefault();
    console.log(formState.inputs);
  };

  if (!identifiedPlace)
    return (
      <div className='center'>
        <Card>
          <h2>Could not find place!</h2>
        </Card>
      </div>
    );

  if (isLoading) {
    return (
      <div className='center'>
        <h2>Loading ...</h2>
      </div>
    );
  }

  return (
    <form className='place-form' onSubmit={placeUpdateSubmitHandler}>
      <Input
        id='title'
        element='input'
        type='text'
        label='Title'
        validators={[VALIDATOR_REQUIRE()]}
        errorText='Please enter a valid title.'
        onInput={inputHandler}
        initialValue={formState.inputs.title.value}
        initialValid={formState.inputs.title.isValid}
      />
      <Input
        id='description'
        element='textarea'
        label='Description'
        validators={[VALIDATOR_MINLENGTH(5)]}
        errorText='Please enter a valid description (min. 5 characters).'
        onInput={inputHandler}
        initialValue={formState.inputs.description.value}
        initialValid={formState.inputs.description.isValid}
      />
      <Button type='submit' disabled={!formState.isValid}>
        UPDATE PLACE
      </Button>
    </form>
  );
}

export default UpdatePlace;
